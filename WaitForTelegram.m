function out=WaitForTelegram(t)
if(isempty(t));t=1;end
out=[];
    while(isempty(out));
       out=TelegramGet();
       pause(t);
    end
end