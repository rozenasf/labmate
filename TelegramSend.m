function output=TelegramSend(varargin)
global L;
if(isempty(L));error('Please Load Parameters.mat');output=1;return;end
switch nargin
    case 0
        text=[];
        Keyboard=[]; 
    case 1
        text=varargin{1};
        Keyboard=[];        
    case 2
        text=varargin{1};
        Keyboard=varargin{2};
end

OriText=text;
BeckupKeyboard=L.Keyboard;

if(exist('Keyboard'));L.Keyboard=Keyboard;end

IsPhoto=0;
if(isempty(L));load('Parameters.mat');end
if(nargin==0);text=[];end;
if(~iscell(text))
    if(nargin==0)
        IMDATA = imclipboard('paste');imwrite(IMDATA,'temp.png');
        output=Photo('temp.png');
        IsPhoto=1;
    else
        if(~isnumeric(text))
            text=breakdown(text,0);
            text=strrep(text,' ','%20');text=strrep(text,'\t','-->');text=strrep(text,'\n','%0A');
            if(numel(L.Keyboard)==0)
                output=urlread(['https://api.telegram.org/bot',L.token,'/sendMessage?chat_id=',L.id,'&text=',text]);
            else
                output=urlread(['https://api.telegram.org/bot',L.token,'/sendMessage?chat_id=',L.id,'&text=',text,'&reply_markup={"keyboard":',Array2Json(L.Keyboard),',"resize_keyboard":true,"one_time_keyboard":true}']);
            end
        else
            if(text==0)
                set(gcf,'PaperOrientation','portrait');
                print('temp','-dpng') ;
            else
                set(text,'PaperOrientation','portrait');
                print(['-f',num2str(text)],'temp','-dpng') ;
            end
            output=Photo('temp.png');
            IsPhoto=1;
        end
    end
    if(L.Bind);
        if(IsPhoto)
            L.Bind=2;
            DocSend();
        else
            
            DocSend(OriText);
        end
    end
else
    for i=1:numel(text)
        TelegramSend(text{i});
    end
end
L.Keyboard=BeckupKeyboard;
end