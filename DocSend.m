function output=DocSend(text)
          global L;
          %if(isempty(L));disp(text);output=1;return;end
          if(isempty(L));error('please load Parameters.mat');end
          if(isempty(L.ActiveNotebook));disp('please set active notebook by SetActiveNotebook');return;end;
          try ftell(L.File{2});catch;SetActiveNotebook(L.ActiveNotebook);end
          if(nargin==0);text=[];end;
          if(~iscell(text))
              
              name=[L.MainPath,L.File{1},'/',num2str(1+size(ls([L.MainPath,L.File{1},'\*.png']),1))];
              namePar_png=[num2str(1+size(ls([L.MainPath,L.File{1},'\*.png']),1)),'.png'];
              name_png=[name,'.png'];
              
              Hight='150';
              if(~isfield(L,'SupressNewLineFigure'))
                  L.SupressNewLineFigure=0;
              else
                  if(L.SupressNewLineFigure)
                      HTML_Image=['<img src="',namePar_png,'" alt=""  height="',Hight,'"> ',' \n'];
                  else
                      HTML_Image=['<img src="',namePar_png,'" alt=""  height="',Hight,'"> ',datestr(now),' <br>\n'];
                  end
              end
              
              if(nargin==0)
                  if(L.Bind==1)
                    IMDATA = imclipboard('paste');imwrite(IMDATA,'temp.png');
                  elseif(L.Bind==2)
                      L.Bind=1;
                  end
                    fprintf(L.File{2},HTML_Image);
                  copyfile('temp.png',name_png);
              else
                  if(~isnumeric(text))
                      oritext=text;
                      text=breakdown(text,0);
                          if(toc(L.File{3})>600);L.File{3}=tic;fprintf(L.File{2},[datestr(now),'<br>\n']);end
                          ToPrint=strrep(strrep(text,'\n','<br>\n'),'\t','&emsp;');
                          IDX1=findstr(ToPrint,':\');IDX2=findstr(ToPrint,'.mat');
                          if(numel(IDX2)==numel(IDX1))
                              for u=numel(IDX1):-1:1
                                link=strrep(ToPrint(IDX1(u)-1:IDX2(u)+3),'\','\\');
                                ToPrint=[ToPrint(1:(IDX1(u)-2)),'<a href="',link,'">',link,'</a>',ToPrint(IDX2(u)+4:end)];
                              end
                          end
                          if(isfield(L,'JSMarker') && L.JSMarker)
                              if(isstruct(oritext) && isfield(oritext,'Title'))
                                  ToPrint=['<div data-role="collapsible"><h1>',strrep(oritext.Title,'\','\\'),'</h1><p>',ToPrint,'</p></div>'];
                              end
                          end
                          fprintf(L.File{2},ToPrint);
                  else
                      %change to img = getframe(gcf);imwrite(img.cdata,
                      %[name, '.png']); sometime
                          if(text==0)
                              set(gcf,'PaperOrientation','portrait');
                              print('temp','-dpng') ;
                          else
                              set(text,'PaperOrientation','portrait');
                              print(['-f',num2str(text)],'temp','-dpng') ;
                          end
                      
                      fprintf(L.File{2},HTML_Image);
                      copyfile('temp.png',name_png);
                  end
              end
          else
              for i=1:numel(text)
                  if(i<numel(text) && isnumeric(text{i}) && isnumeric(text{i+1})); L.SupressNewLineFigure=1; else; L.SupressNewLineFigure=0; end
                  DocSend(text{i});
              end
          end