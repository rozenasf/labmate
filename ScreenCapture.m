function outputImage=ScreenCapture(subRegion,outputFile)

    if exist('subRegion','var') && ~isempty(subRegion)
        x=subRegion(1);
        y=subRegion(2);
        w=subRegion(3);
        h=subRegion(4);
    end

    robo = java.awt.Robot;

    if ~exist('subRegion','var') | isempty(subRegion)
        t = java.awt.Toolkit.getDefaultToolkit();
        rectangle = java.awt.Rectangle(t.getScreenSize());
    else
        rectangle = java.awt.Rectangle(x,y,w,h);
    end
    image1 = robo.createScreenCapture(rectangle);
    
    if nargout
        h=image1.getHeight();
        w=image1.getWidth();
        data=image1.getData();
        pix=data.getPixels(0,0,w,h,[]);
        tmp=reshape(pix(:),3,w,h);
        for ii=1:3
            outputImage(:,:,ii)=squeeze(tmp(ii,:,:))';
        end
    end
    
    if exist('outputFile','var')
        filehandle = java.io.File(outputFile);
        javax.imageio.ImageIO.write(image1,'jpg',filehandle);
    end
end