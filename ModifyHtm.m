function Htm=ModifyHtm(Htm,adress,varargin)
    Location=['Htm{',num2str(adress(1)),'}'];
    for i=2:numel(adress)
        Location=[Location,'.data{',num2str(adress(i)),'}'];
    end
    Struct=eval([Location,';']);
    for i=1:numel(varargin)/2
        Struct.(varargin{2*i-1})=varargin{2*i};
    end
    eval([Location,'=Struct;']);
end