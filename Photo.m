function output=Photo(path)
global L;
f=fopen(path);d=fread(f,Inf);fclose(f);
output = urlreadpost(['https://api.telegram.org/bot',L.token,'/sendPhoto'],{'chat_id',L.id,'photo',d});
end