function Json=JsonFromFull(full)
Json=[];
IDX=[findstr(full,'{"update_id"'),numel(full)];
Parameters={'update_id','message_id','id','first_name','last_name','date','text','file_id'};
for i=1:numel(IDX)-1
    for j=1:numel(Parameters)
        Json{i}.(Parameters{j})=CutTextToComma(full(IDX(i):IDX(i+1)-3),Parameters{j});
    end
end
end