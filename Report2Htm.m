function Htm=Report2Htm(Report)
SectionTitleStyle=MakeStyle(struct('color','red','size',14));

BlankHolder='&nbsp;';

Htm=[];
Htm{1}=struct('parameters','','name','html');
o=1;
if(isfield(Report,'Head'))
Htm{1}.data{o}=struct('parameters','','name','head','data',Report.Head);o=o+1;
end
Htm{1}.data{o}=struct('parameters','lang=EN-US','name','body');
Htm{1}.data{o}.data{1}=struct('parameters','class=WordSection1','name','div');
j=1;
for i=1:numel(Report.Name)
    Htm{1}.data{o}.data{1}.data{j}=struct('parameters','','name','p');
    Htm{1}.data{o}.data{1}.data{j}.data{1}=struct('parameters',SectionTitleStyle,'name','span','data',Report.Name{i});
    j=j+1;
    Htm{1}.data{o}.data{1}.data{j}=struct('parameters','border="1"','name','table');
    for k=1:size(Report.Text{i},1)
        Htm{1}.data{o}.data{1}.data{j}.data{k}=struct('parameters','','name','tr');
        for l=1:size(Report.Text{i},2)
            Htm{1}.data{o}.data{1}.data{j}.data{k}.data{l}=struct('parameters','','name','td');
            for m=1:numel(Report.Text{i}{k,l})
                Htm{1}.data{o}.data{1}.data{j}.data{k}.data{l}.data{m}=struct('parameters','','name','p');
                text=Report.Text{i}{k,l}{m};if(isempty(text));text=BlankHolder;end
                Htm{1}.data{o}.data{1}.data{j}.data{k}.data{l}.data{m}.data{1}=...
                struct('parameters',Report.Parameters{i}{k,l}{m},'name','span','data',text);
            end
        end
    end
    j=j+1;
end