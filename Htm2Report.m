function Report=Htm2Report(Htm)
    Tables=[];
    Htm2=Htm{1}.data{2}.data{1}.data;
    for i=2:numel(Htm2)
        if(strcmp(Htm2{i}.name,'table'));
            TableName=DigIn(Htm2{i-1}.data);
            Tables{end+1}.name=TableName.data;
            Tables{end}.Adress=[1,2,1,i];
            Tables{end}.Htm=Htm2{i}.data;
        end
    end
    Report=[];
    Report.Head=Htm{1}.data{1}.data;
    for i=1:numel(Tables)
       Report.Name{i}=Tables{i}.name;
       for j=1:(numel(Tables{i}.Htm))
           for k=1:(numel(Tables{i}.Htm{1}.data))
               for l=1:numel(Tables{i}.Htm{j}.data{k}.data)
                Report.Text{i}{j,k}{l}= Tables{i}.Htm{j}.data{k}.data{l}.data{1}.data;
                if(iscell(Tables{i}.Htm{j}.data{k}.data{l}.data{1}.data));
                    disp(['warning ',num2str([i, j, k, l])]);
                else
                    Report.Text{i}{j,k}{l}=strrep(Report.Text{i}{j,k}{l},'&nbsp;','');
                end
                Report.Parameters{i}{j,k}{l}= Tables{i}.Htm{j}.data{k}.data{l}.data{1}.parameters;
                
               end
           end
       end
    end
end