function CheckAndExecuteCommand(text)
global L;
    if(L.TableFlag)
        HtmPath=[L.MainPath,L.ActiveNotebook,'/Table.Htm'];
        out=dir(HtmPath);
        if(~(out.datenum==L.Tabledatenum));
            L.d=ReadHtmFile(HtmPath);
            L.Htm=FilletHtm(L.d);
            L.Report=Htm2Report(L.Htm);
            L.Tabledatenum=out.datenum;
        end;
        % For BotFather
% notebook - select notebook
% section - select section
% subsection - select sub section
% instructions - get step instruction
% options - load options keyboard
        switch(text)
            case '/notebook'
                q=dir(L.MainPath);
                L.Keyboard=[];
                for i=3:numel(q)
                    L.Keyboard{i-2,1}=q(i).name;
                end
                TelegramSend('Which Notebook?');L.Keyboard=[];
                out=WaitForTelegram(2);
                SetActiveNotebook(out);
                TelegramSend('OK Notebook');
            case '/section'
                L.Keyboard=L.Report.Name';
                TelegramSend('Which Section?');out=WaitForTelegram(2);
                I=find(strcmp(KeepOnlyLetters(L.Keyboard),KeepOnlyLetters(out)));L.Keyboard=[];
                if(numel(I)==1);L.StepInTable(1)=I;else disp(['problem, unrecognized ',out]);return;end
                TelegramSend('OK Section');
                CheckAndExecuteCommand('/subsection');
            case '/subsection'
                for i=2:size(L.Report.Text{L.StepInTable(1)})
                    L.Keyboard{i-1,1}=L.Report.Text{L.StepInTable(1)}{i,1}{1};
                end
                TelegramSend('Which SubSection?');out=WaitForTelegram(2);
                J=find(strcmp(KeepOnlyLetters(L.Keyboard),KeepOnlyLetters(out)));L.Keyboard=[];
                if(numel(J)==1);L.StepInTable(2)=J+1;else disp(['problem, unrecognized ',out]);return;end
                TelegramSend('OK SubSection');
                CheckAndExecuteCommand('/instructions');
                CheckAndExecuteCommand('/options');
            case '/instructions'
                TelegramSend(['Section :',L.Report.Name{L.StepInTable(1)}]);
                TelegramSend(['SubSection :',L.Report.Text{L.StepInTable(1)}{L.StepInTable(2),1}{1}]);
                for i=1:numel(L.Report.Text{L.StepInTable(1)}{L.StepInTable(2),2})
                    TelegramSend(L.Report.Text{L.StepInTable(1)}{L.StepInTable(2),2}{i});
                end
            case '/options'
                L.Keyboard={'<--','~DONE~','-->';'~Comment~','~UnDone~','~LastSeen~'};
                TelegramSend('Options:');
            case '~Comment~'
                L.Keyboard=[];
                TelegramSend(['What is your Comment on ',L.Report.Name{L.StepInTable(1)}, ' @ ',L.Report.Text{L.StepInTable(1)}{L.StepInTable(2),1}{1},' ?']);
                out=WaitForTelegram(2);
                L.Report.Text{L.StepInTable(1)}{L.StepInTable(2),3}{1}=[datestr(datetime),':',out];
                L.Htm=Report2Htm(L.Report);WriteHtmFile(L.Htm,HtmPath);
                TelegramSend('Comment Saved');
            case '~DONE~'
                L.Report.Parameters{L.StepInTable(1)}{L.StepInTable(2),1}{1}=MakeStyle(struct('color','black','size',8,'background','lime'));
                L.Htm=Report2Htm(L.Report);WriteHtmFile(L.Htm,HtmPath);
                TelegramSend('Done OK');
            case '<--'
                
                L.StepInTable(2)=L.StepInTable(2)-1;
                if(L.StepInTable(2)<2);L.StepInTable(2)=2;end
                CheckAndExecuteCommand('/instructions')
                
            case '-->'
                Max=size(L.Report.Text{L.StepInTable(1)},1);
                L.StepInTable(2)=L.StepInTable(2)+1;
                if(L.StepInTable(2)>Max);L.StepInTable(2)=Max;end
                CheckAndExecuteCommand('/instructions')
                
            case '~UnDone~'
                L.Report.Parameters{L.StepInTable(1)}{L.StepInTable(2),1}{1}=MakeStyle(struct('color','black','size',8));
                L.Htm=Report2Htm(L.Report);WriteHtmFile(L.Htm,HtmPath);
                TelegramSend('UnDone OK');
            case '~LastSeen~'
                L.Keyboard=[];
                TelegramSend('Where is it?');
                out=WaitForTelegram(2);
                L.Report.Text{1}{size(L.Report.Text{L.StepInTable(1)},1),2}{1}=[datestr(datetime),':',out];
                L.Htm=Report2Htm(L.Report);WriteHtmFile(L.Htm,HtmPath);
            otherwise
                return;
        end
        save('Parameters.mat','L');
    end           
end