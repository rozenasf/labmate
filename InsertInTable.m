function OK=InsertInTable(Section,Position,text,Path)
Span=['<span style=',char(39),'font-size:8.0pt;font-family:"Times New Roman",serif;color:black',char(39),'>'];
EndSpan=['</span>'];
[Hie]=ParseHtm(Path);
OK=1;
    names=fieldnames(Hie.data);
    Idx=find(strcmp(names,Section));
    if(numel(Idx)~=1);OK=0;return;end
    ini=Hie.PlaceHolders{Idx}.Global(Position(1)*3-3+Position(2),1);
    fin=Hie.PlaceHolders{Idx}.Global(Position(1)*3-3+Position(2),2);
    text=strrep(text,'\n',[EndSpan,'</p><p>',Span]);
    data=[Hie.Raw(1:ini-1)','<p>',Span,text,EndSpan,'</p>',Hie.Raw(fin+1:end)']';
    f2=fopen(Path,'w');fwrite(f2,data);fclose(f2);
end