function [out,full,Json]=TelegramGet()
% commands:
% retract - Retract and zero scanners
% zero - Zero all voltages
% stop - stop execution of code by creating an error
% emergency - zero -> retract -> stop sequance
global L;
if(isempty(L));load('Parameters.mat');end
try
    full=urlread(['https://api.telegram.org/bot',L.token,'/getUpdates?offset=',num2str(L.update_id)]);
    Json=JsonFromFull(full);
    if(isempty(Json));out=[];return;end
    for i=1:numel(Json)
        out=GetTextPhotoFromJson(Json{i});
        %out might be an image
        if(L.Bind==1);
            if(numel(size(out))==3)
                imwrite(out,'temp.png');
                L.Bind=2;
                DocSend();
                L.update_id=Json{end}.update_id+1;
            else
                DocSend(out);
                L.update_id=Json{end}.update_id+1;
%                 try
%                     CheckAndExecuteCommand(out);
%                 catch
%                    disp('problem with Execute Command'); 
%                 end
            end
        else
            if(numel(size(out))==3)
                %imwrite(out,'temp.png');
                %L.Bind=2;
                %DocSend();
                L.update_id=Json{end}.update_id+1;
            else
                %DocSend(out);
                L.update_id=Json{end}.update_id+1;
%                 try
%                     CheckAndExecuteCommand(out);
%                 catch
%                    disp('problem with Execute Command'); 
%                 end
            end            
        end
    end
    
     urlread(['https://api.telegram.org/bot',L.token,'/sendMessage?chat_id=',L.id,'&text=','OK']);
catch ME
    out=[];
    full=[];
    disp('problem reciving message');
    rethrow(ME)
end
end