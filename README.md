# README #

Links to Issues  
Links to Wiki guide  
Links to Wiki Quick start examples  

### What is this repository for? ###

* Telegram communication and automatic documentation  
* Version V1  

### Who do I talk to? ###

* Asaf Rozen

### BASIC Commands ###

# output data structure:#
* text - send the text  
* empty - send the image in clipboard  
* number - send the figure with that number  
* 0 - send the active figure  
* variable of type structure - send the full variable
*   you can specify a keyboard for easy reply. for example: TelegramSend('Hi',{'Yes','No';'good',bad})
* you can also do a couple of actions. example: DocSend{'HI',1,2,3,Info}  

# commands #

* DocSend('Example')
* TelegramSend('Example')
* TelegramSend('Hi',{'Yes','No';'good',bad})
* TelegarmGet();
* TelegramGetLoop(); <-- same as TelegarmGet but waits untill input

### BASIC START ###

* global variable L that include:  
                   token: '4XXXXXXX0:AAGCtQ8jXsaZiqXEoF5N53g1S3F5e0OnbBM'  
                      id: '4XXXXXX0'  
               update_id: 731889001  
                MainPath: 'Q:\Measurements\Asaf R\Mating\'  
                Keyboard: []  
          ActiveNotebook: 'AsafR4B4_311017'  
                    File: {'AsafR4B4_311017'  [7]  [28291444159375]}  
                    Bind: 1  
            Tabledatenum: 7.3694e+05  
               TableFlag: 0  
    SupressNewLineFigure: 0  
* the "token" is the "telegram bot" identification.  
* the "id" is your phone telegram user id  
* the "main path" is the main folder for documentation  
* the ActiveNotebook is the name of the subfolder in MainPath for the notebook.  
* the file is the handle for the notbook. in order to change notebook, change the ActiveNotebook or MainPath and that L.File=[]; it will update automatically  
* the Bind determends if data from bot and to bot will be documented 0/1  

### How to generate a bot? ###

1. Install telegram app  
2. you want to use the web version of telegram, will be easier. open it on chrome. this stage optional  
2. search for a contact named "botfather".   
3. press "start" (if this is the first communication with the botfather)  
4. send /newbot  
5. choose a name, for example AsafLabBot, and send it  
6. choose a username, it must end with "bot". for example, "AAABBBbot", this will not be important ever, send it.  
7. now, the bot father will say: "Done! ..." now search for the line that looks like: "497858717:AAE1AUZLqjP6EE8HrozpkqfRZ7xzxZLQ3FM". this it the bot "token".  
8. search a contact name with your bot name and sent it somthing  
9. write the url: https://api.telegram.org/bot + "Token"+/getUpdates  in a new tab. for example: https://api.telegram.org/bot497858717:AAE1AUZLqjP6EE8HrozpkqfRZ7xzxZLQ3FM/getUpdates  
10. copy the id. this is your id.  
DONE!  