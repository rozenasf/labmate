function out=GetTextPhotoFromJson(Json)
global L;
if(Json.id==str2num(L.id))
    if(~isempty(Json.text))
        %if(findstr(Json.text,'\'));Json.text=SystemCommands(Json.text);end
%        if(~isempty(L.File));fprintf(L.File{2},[strrep(Json.text,'\n','<br>\n'),'<br>\n']);end
        out=Json.text;
        disp(out);
    else
        
        FileLocation=urlread(['https://api.telegram.org/bot',L.token,'/getFile?file_id=',Json.file_id]);
        IDX=findstr('"file_path"',FileLocation);
        file_path=FileLocation(IDX+13:end-3);
        [out]=imread(['https://api.telegram.org/file/bot',L.token,'/',file_path]);
%         if(L.flag_File)
%             name=[L.MainPath,L.File{1},'/',num2str(1+size(ls([L.MainPath,L.File{1},'\*.png']),1))];
%             namePar_png=[num2str(1+size(ls([L.MainPath,L.File{1},'\*.png']),1)),'.png'];
%             name_png=[name,'.png'];
%             imwrite(out,name_png);
%             HTML_Image=['<img src="',namePar_png,'" alt=""  height="300"> ',datestr(now),' <br>\n'];
%             fprintf(L.File{2},HTML_Image);
%         end
        disp('Got Image');
    end
else
    out=[];
    text=sprintf('%s %s is trying to communicate with bot, he said: text - %s, file_id - %s\n',Json.first_name,Json.last_name,Json.text,Json.file_id);
    urlread(['https://api.telegram.org/bot',L.token,'/sendMessage?chat_id=',L.id,'&text=',text]);
    disp(text);
end
end