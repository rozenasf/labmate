function ClipSend(in)
global Info;
handles=findall(0,'type','figure');
Numbers=[handles.Number];
if(~exist('in'));in=Numbers(1:find(Numbers==999)-1);end
Xmax=0;YAcum=0;
F={};
for i=1:numel(in)
    F{i}=getframe(handles(in(i)==Numbers));
    F{i}=F{i}.cdata;
    YAcum=YAcum+size(F{i},1);
    Xmax=max([Xmax,size(F{i},2)]);
end

try 
    Description=sprintf(' Scan %d: %s',Info.ScanNumber,Info.Scan.Description);
    Shift=64;
    x=255*ones(YAcum+Shift,Xmax,3,'uint8');
    x= insertText(x,[0,0],Description,'FontSize',36);
catch
    Shift=0;
    x=255*ones(YAcum+Shift,Xmax,3,'uint8');
end
j=1;
k=1;
for i=1:numel(F)
   
    x(Shift+j:size(F{i},1)+Shift+j-1,1:size(F{i},2),:)=F{i};
    j=j+size(F{i},1);
end

% XAcum=0;Ymax=0;
% F={};
% for i=1:numel(in)
%     F{i}=getframe(handles(in(i)==Numbers));
%     F{i}=F{i}.cdata;
%     XAcum=XAcum+size(F{i},2);
%     Ymax=max([Ymax,size(F{i},1)]);
% end
% 
% try 
%     Description=sprintf(' Scan %d: %s',Info.ScanNumber,Info.Scan.Description);
%     Shift=64;
%     x=255*ones(Ymax+Shift,XAcum,3,'uint8');
%     x= insertText(x,[0,0],Description,'FontSize',36);
% catch
%     Shift=0;
%     x=255*ones(Ymax+Shift,XAcum,3,'uint8');
% end

% j=1;
% for i=1:numel(F)
%     x(Shift+1:size(F{i},1)+Shift,j:j+size(F{i},2)-1,:)=F{i};
%     j=j+size(F{i},2);
% end

imclipboard('copy', x)
end