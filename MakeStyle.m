function out=MakeStyle(in)
    Parameters=struct('size','8','color','black');
    if(exist('in') && isstruct(in))
        names=fieldnames(in);
        for i=1:numel(names)
            Parameters.(names{i})=in.(names{i});
        end
    end
    out=['style=',char(39),'font-size:',num2str(Parameters.size),'.0pt;font-family:"Times New Roman",serif; color:',...
        Parameters.color];
    if(isfield(Parameters,'background'))
        out=[out,';background:',Parameters.background];
    end
    out=[out,char(39)];
end