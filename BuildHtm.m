function out=BuildHtm(Htm)
out=[];n=char(10);
if(iscell(Htm))
    %names=fieldnames(Htm);names=setdiff(names,'parameters');
    for i=1:numel(Htm)
        if(iscell(Htm{i}.data));
            if(~isempty(Htm{i}.parameters))
                out=[out,'<',Htm{i}.name,' ',Htm{i}.parameters,'>',n,BuildHtm(Htm{i}.data),'</',Htm{i}.name,'>',n];
            else
                out=[out,'<',Htm{i}.name,'>',n,BuildHtm(Htm{i}.data),'</',Htm{i}.name,'>',n];
            end
            continue;
        else
            if(~isempty(Htm{i}.parameters))
                out=[out,'<',Htm{i}.name,' ',Htm{i}.parameters,'>',n,Htm{i}.data,n,'</',Htm{i}.name,'>',n];
            else
                out=[out,'<',Htm{i}.name,'>',n,Htm{i}.data,n,'</',Htm{i}.name,'>',n];
            end
        end
%         for j=1:numel(Htm.(names{i}))
%             if(isfield(Htm.(names{i}){j},'parameters'))
%                 par=Htm.(names{i}){j}.parameters;
%             else
%                 par='';
%             end
%             if(~isempty(par))
%                 out=[out,'<',names{i},' ',par,'>',n,BuildHtm(Htm.(names{i}){j}),'</',names{i},'>',n];
%             else
%                 out=[out,'<',names{i},'>',n,BuildHtm(Htm.(names{i}){j}),'</',names{i},'>',n];
%             end
%         end
    end
end
end