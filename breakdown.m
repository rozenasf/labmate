     function [Text]=breakdown(in,Shift)
     global L;
          Text=[];
          if(isstruct(in))
              Text=['\n',repmat('\t',1,Shift)];
              names=fieldnames(in);
              for i=1:numel(names)
                  [Temp_Text]=[names{i},':',breakdown(in.(names{i}),Shift+1)];
                    Text=[Text,',',Temp_Text];
              end
              Text=[Text(1:end),'\n',repmat('\t',1,Shift-1)];
          elseif(iscell(in))
              Text=['\n',repmat('\t',1,Shift)];
              for i=1:numel(in)
                  [Temp_Text]=['{',num2str(i),'}',':',breakdown(in{i},Shift+1)];
                  Text=[Text,',',Temp_Text];
              end
              Text=[Text(1:end),'\n',repmat('\t',1,Shift-1)];
          elseif(isnumeric(in) && ~isempty(in))
              if(numel(in)<10 && numel(in)==max(size(in)))
                  if(numel(in)==1)
                      Text=[Text,num2str(in,'%10.5e')];
                  else
                      if(min(size(in))==1)
                          Text=[Text,'[',num2str(in(:)','%10.5e,'),']'];
                      else
                          Text=[Text,'['];
                          for j=1:(size(in,1)-1)
                              Text=[Text,num2str(in(j,:),'%10.5e,')];
                          end
                          Text=[Text,num2str(in(end,:),'%10.5e,'),']'];
                      end
                      
                  end
              else
                  X=size(in);
                  if(X(1)==1)
                      Text=[Text,sprintf('[%e TO %e , %d] ',in(1),in(end),numel(in))];
                  else
                      Text=[Text,'['];
                      for j=1:(numel(X)-1)
                          Text=[Text,num2str(X(j)),'X'];
                      end
                      Text=[Text,num2str(X(end)),']'];
                  end
              end
          elseif(ischar(in))
              if(numel(in)>0)
                  Text=[Text,in];
              end
          elseif(isa(in,'function_handle'))
              Text=[Text,func2str(in)];
          else
              Text=[Text,class(in)];
          end
          
          if(Shift==0);
              
              Text=strrep(Text,'\t,','\t');Text=strrep(Text,'\n,','\n');
              for e=1:4
                Text=strrep(Text,['\n',repmat('\t',1,e),'\n'],'\n');
              end
              if(numel(Text)>1 && strcmp(Text(1:2),'\n'));
                  Text=Text(3:end);
              end
              if(numel(Text)>1 && ~strcmp(Text(end-1:end),'\n'));
                Text=[Text,'\n'];
              end
          end
      end