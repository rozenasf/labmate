function OK=HighlightLine(Section,Position,Line,Color,Path)
%lime
if(isempty(Color))
    Span=['<span style=',char(39),'font-size:8.0pt;font-family:"Times New Roman",serif;color:black',char(39),'>'];
else
    Span=['<span style=',char(39),'font-size:8.0pt;font-family:"Times New Roman",serif;color:black;background:',Color,';mso-highlight:',Color,char(39),'>'];
end
EndSpan=['</span>'];

[Hie]=ParseHtm(Path);
OK=1;
    names=fieldnames(Hie.data);
    Idx=find(strcmp(names,Section));
    if(numel(Idx)~=1);OK=0;return;end
    if(size(Hie.PlaceHolders{Idx}.Local{Position(1)*3-3+Position(2)},1)<Line);OK=0;return;end
    ini=Hie.PlaceHolders{Idx}.Local{Position(1)*3-3+Position(2)}(Line,1);
    fin=Hie.PlaceHolders{2}.Local{Position(1)*3-3+Position(2)}(Line,2);
    text=Hie.data.(Section){Position(1),Position(2)}{Line}.text;
    text=strrep(text,'\n',[EndSpan,'</p><p>',Span]);
    data=[Hie.Raw(1:ini-1)','<p>',Span,text,EndSpan,'</p>',Hie.Raw(fin+1:end)']';
    f2=fopen(Path,'w');fwrite(f2,data);fclose(f2);
    
end