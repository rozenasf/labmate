function filename = validatePasteInput(varargin)

if nargin == 0
    filename = '';
else
    filename = varargin{1};
    validateattributes(filename, {'char'}, {'row'}, mfilename, 'FILENAME');
    [~, ~, e] = fileparts(filename);
    if ~ismember(lower(e), ...
            {'.jpg', '.jpeg', '.tif', '.tiff', '.png', '.gif', '.bmp'})
        error('imclipboard:InvalidFileFormat', ...
            'You must specify a valid image extension: .jpg, .tif, .png, .gif, .bmp');
    end
end

end