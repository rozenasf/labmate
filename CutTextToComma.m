function out=CutTextToComma(in,word)
out=[];
IDX1=findstr(in,['"',word,'"']);
if(~isempty(IDX1))
    temp=in((IDX1(end)+numel(word)+3):(end));
    %IDX2=find((temp==',') + (temp=='}'));
    IDX2=sort([strfind(temp,[',',10]),strfind(temp,',"'),strfind(temp,'}')]);
    out=(temp(1:(IDX2(1)-1)));
    out(findstr(out,'"'))=[];
    if(all(ismember(out, '0123456789')));out=str2num(out);end
end
end