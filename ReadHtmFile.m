function d=ReadHtmFile(Path)
f=fopen(Path);d=fread(f,Inf);fclose(f);
d=char(d');d=strrep(d,[char(13),char(10)],' ');
d=d(ismember(d,['abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890 !"#%&()*+,-./0123456789:;<=>@[]_~"',char(39)]));

end