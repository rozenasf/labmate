function Json=Array2Json(Array)
Json='[';
    for i=1:size(Array,1)
        Json=[Json,'['];
        for j=1:size(Array,2)
            if(numel(Array{i,j})>0)
            Json=[Json,'"',Array{i,j},'"',','];
            else
                Json=[Json];
            end
        end
        Json=[Json(1:end-1),'],'];
    end
Json=[Json(1:end-1),']'];    
end