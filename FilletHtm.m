function [out]=FilletHtm(Text)

        i=1;out=[];
    while(1)
        NoPar=0;
        Idx=strfind(Text,'<');if(numel(Idx)==0);break;end;Idx=Idx(1);
        Idx2=strfind(Text,'>');Idx2=Idx2(find(Idx2>Idx,1));
        Space=strfind(Text,' ');Space=Space(find(Space>Idx,1));
        if(isempty(Space) || Space>Idx2);Space=Idx2;NoPar=1;end
        name{i}=Text(Idx+1:Space-1);
        if(~NoPar);parameters{i}=Text(Space+1:Idx2-1);else parameters{i}='';end
        Closing=strfind(Text,['</',name{i}]);Closing=Closing(find(Closing>Idx,1));
        SubTexts{i}=Text(Idx2+1:Closing-1);
        FinalClosing=strfind(Text,'>');FinalClosing=FinalClosing(find(FinalClosing>Closing,1));
        if(FinalClosing+1>numel(Text));break;end
        i=i+1;
        Text=Text(FinalClosing+1:end);
    end
    for i=1:numel(SubTexts)
        Idx=strfind(SubTexts{i},'<');
       
        if(numel(Idx)==0 || isequal(name{i},'head'));
            out{i}.data=SubTexts{i};
            out{i}.parameters=parameters{i};
            out{i}.name=name{i};
        else
            [out2]=FilletHtm(SubTexts{i});
            out{i}.data=out2;
            out{i}.parameters=parameters{i};
            out{i}.name=name{i};
        end
    end
end