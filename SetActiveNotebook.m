function SetActiveNotebook(Name)
global L;
if(isempty(L));load('Parameters.mat');end
L.ActiveNotebook=Name;
if(~isfield(L,'JSMarker'));L.JSMarker=0;end
if(size(ls([L.MainPath,L.ActiveNotebook]),1)==0);mkdir([L.MainPath,L.ActiveNotebook]);end
if(~isempty(ls([L.MainPath,L.ActiveNotebook,'/Table.Htm'])))
    L.TableFlag=1;
    HtmPath=[L.MainPath,L.ActiveNotebook,'/Table.Htm'];
    out=dir(HtmPath);
    L.Tabledatenum=out.datenum;
    L.d=ReadHtmFile(HtmPath);
    L.Htm=FilletHtm(L.d);
    L.Report=Htm2Report(L.Htm);
else
    L.TableFlag=0;
end
f=fopen([L.MainPath,L.ActiveNotebook,'/',L.ActiveNotebook,'.html'],'a+');
L.File= {L.ActiveNotebook,f,tic};
if(L.JSMarker)
     AddJS();
end
save('Parameters.mat','L');
save([L.MainPath,L.ActiveNotebook,'/','Parameters.mat'],'L');
end