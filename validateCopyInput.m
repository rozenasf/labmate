function data = validateCopyInput(varargin)

if nargin == 0
    error('imclipboard:NotEnoughArguments', ...
        'For ''copy'', the second argument must be an image array');
end

if nargin == 2  % X and colormap
    % X
    validateattributes(varargin{1}, {'double', 'uint8', 'uint16'}, {'2d', 'real', 'nonnegative'}, mfilename, 'X');
    if isa(varargin{1}, 'double')
        validateattributes(varargin{1}, {'double'}, {'>=', 0, '<=', 1}, mfilename, 'X');
    end
    
    % MAP
    validateattributes(varargin{2}, {'double'}, {'size', [nan 3], '>=', 0, '<=', 1}, mfilename, 'MAP');
    data = ind2rgb(varargin{1}, varargin{2});
    data = uint8(255*data);

else % nargin == 1 % Grayscale, True Color, Black and White
        validateattributes(varargin{1}, ...
        {'double', 'logical', 'uint8', 'uint16'}, ...
        {'real', 'nonnegative'}, mfilename, 'IMDATA');

    switch class(varargin{1})
        case 'double'
            validateattributes(varargin{1}, {'double'}, {'>=', 0, '<=', 1}, mfilename, 'IMDATA');
            if ndims(varargin{1}) == 2  % grayscale
                data = uint8(255*varargin{1});
                data = cat(3, data, data, data);
            elseif ndims(varargin{1}) == 3
                validateattributes(varargin{1}, {'double'}, {'size', [nan nan 3]}, mfilename, 'IMDATA');
                data = uint8(255*varargin{1});
            else
                error('imclipboard:InvalideDoubleImage', ...
                    'Double image data must be 2 or 3 dimensions');
            end
            
        case 'logical'
            validateattributes(varargin{1}, {'logical'}, {'2d'}, mfilename, 'IMDATA');
            data = uint8(255*varargin{1});
            data = cat(3, data, data, data);
            
        case 'uint8'
            if ndims(varargin{1}) == 2
                data = cat(3, varargin{1}, varargin{1}, varargin{1});
            elseif ndims(varargin{1}) == 3
                validateattributes(varargin{1}, {'uint8'}, {'size', [nan nan 3]}, mfilename, 'IMDATA');
                data = varargin{1};
            else
                error('imclipboard:InvalideUINT8Image', ...
                    'UINT8 image data must be 2 or 3 dimensions');
            end
            
        case 'uint16'
            if ndims(varargin{1}) == 2
                data = uint8(varargin{1}*(double(intmax('uint8'))/double(intmax('uint16'))));
                data = cat(3, data, data, data);
            elseif ndims(varargin{1}) == 3
                validateattributes(varargin{1}, {'uint16'}, {'size', [nan nan 3]}, mfilename, 'IMDATA');
                data = uint8(varargin{1}*(double(intmax('uint8'))/double(intmax('uint16'))));
            else
                error('imclipboard:InvalideUINT16Image', ...
                    'UINT16 image data must be 2 or 3 dimensions');
            end
            
    end
    
end

end