function out=KeepOnlyLetters(in)
out=[];
    if(iscell(in))
       for i=1:numel(in)
          out{i}= KeepOnlyLetters(in{i});
       end
    else
        out=in(ismember(in,'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890'));
    end
end