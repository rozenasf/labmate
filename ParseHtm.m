function [Hie]=ParseHtm(Path)
f=fopen(Path);d=fread(f,Inf);fclose(f);
%% GetHierarchy

StartTable=findstr(d','<table');
EndTable=findstr(d','</table');
for i=1:numel(StartTable)
    Table{i}=d(StartTable(i):EndTable(i));
end
%%
RelevantTables=[];NewRow=[];NewRowEnd=[];NewCoulum=[];NewCoulumEnd=[];
for i=1:numel(Table)
    NewRow{i}=findstr(Table{i}','<tr');
    NewRowEnd{i}=findstr(Table{i}','</tr');
    NewCoulum{i}=findstr(Table{i}','<td');
    NewCoulumEnd{i}=findstr(Table{i}','</td');
    if(numel(NewCoulum{i})/numel(NewRow{i})==3);RelevantTables=[RelevantTables,i];end
end
%%
Sections=[];
for i=1:numel(RelevantTables)
    o=StartTable(RelevantTables(i));
    t2=char(d(o-1:-1:o-100)');
    L1=findstr(t2,'>');L1=L1(2:end);
    L2=findstr(t2,'<');
    L2=L2(1:numel(L1));
    Idx=find((L1-L2)~=1);Idx=Idx(1);
    Sections(i).name=fliplr(t2(L2(Idx)+1:L1(Idx)-1));
end
%%
for i=1:numel(RelevantTables);
    Index=RelevantTables(i);
    Sections(i).PlaceHolders=[];Sections(i).PlaceHolders.Global=[];
    for j=1:numel(NewCoulum{Index})
        Sections(i).Cell{j}=Table{Index}(NewCoulum{Index}(j):NewCoulumEnd{Index}(j));
        text=Sections(i).Cell{j}(1:end-1)';
        
        L1=findstr(text,'<p');raw_closer=findstr(text,'>');
        L2=findstr(text,'</p');
        Sections(i).PlaceHolders.Global=[Sections(i).PlaceHolders.Global;[StartTable(Index)+NewCoulum{Index}(j)-1+raw_closer(1),StartTable(Index)+NewCoulumEnd{Index}(j)-1-1]];
        closer=[];Par=[];
        Sections(i).PlaceHolders.Local{j}=[];
        for l=1:numel(L1)
            closer(l)=raw_closer(min(find(raw_closer-L1(l)>0)));
            Par{l}.text=text(closer(l)+1:L2(l)-1);
            Sections(i).PlaceHolders.Local{j}=[Sections(i).PlaceHolders.Local{j};[StartTable(Index)+NewCoulum{Index}(j)-1+closer(l),StartTable(Index)+NewCoulum{Index}(j)-3+L2(l)]];
            
            SpanStart=findstr(Par{l}.text,'<span');
            if(numel(SpanStart)>0)
            SpanStart=SpanStart(1);
            SpanEnd=findstr(Par{l}.text,'>');SpanEnd=SpanEnd(min(find(SpanEnd>SpanStart)));
            Par{l}.Span=char(Par{l}.text(SpanStart:SpanEnd));
            else
                Par{l}.Span='';
            end
            L3=findstr(Par{l}.text,'<');L4=findstr(Par{l}.text,'>');
            for k=numel(L3):-1:1
                Par{l}.text(L3(k):L4(k))=[];
            end
            Par{l}.text(((Par{l}.text==10) + (Par{l}.text==13))>0)=[];
            Par{l}.text=char(Par{l}.text);
            Par{l}.text=strrep(Par{l}.text,'&nbsp;','');
            Par{l}.text=strrep(Par{l}.text,'&quot;','"');
        end
        Sections(i).Block{j}=Par;
        
    end
end
%%
Hie=[];
Hie.Raw=d;
for i=1:numel(Sections)
    name=strrep(Sections(i).name,' ','_');
    name=name(ismember(name,'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890_'));
    Hie.data.(name)=reshape(Sections(i).Block,3,numel(Sections(i).Block)/3)';
    Hie.PlaceHolders{i}.Global=Sections(i).PlaceHolders.Global;
    Hie.PlaceHolders{i}.Local=Sections(i).PlaceHolders.Local;
end
end